package cz.cvut.fit.miadp.mvcgame.model.gameObject;

import cz.cvut.fit.miadp.mvcgame.state.DoubleShootingMode;
import cz.cvut.fit.miadp.mvcgame.state.IShootingMode;
import cz.cvut.fit.miadp.mvcgame.state.SingleShootingMode;
import cz.cvut.fit.miadp.mvcgame.strategy.RealisticMoveStrategy;
import cz.cvut.fit.miadp.mvcgame.strategy.SimpleMoveStrategy;
import cz.cvut.fit.miadp.mvcgame.visitor.IGameObjectsVisitor;

import java.util.List;

public abstract class AbsCannon extends GameObject {
//    private int power;
    protected IShootingMode shootingMode;

    private static IShootingMode SINGLE_SHOOTING_MODE = new SingleShootingMode();
    private static IShootingMode DOUBLE_SHOOTING_MODE = new DoubleShootingMode();

    public abstract void moveUp();
    public abstract void moveDown();

    public abstract void aimDown();
    public abstract void aimUp();

    public abstract void powerUp();
    public abstract void powerDown();

    public abstract List<AbsMissile> shoot();
    public abstract void primitiveShoot();
    public void toggleShootingMode(){
        if (this.shootingMode instanceof SingleShootingMode) {
            this.shootingMode = DOUBLE_SHOOTING_MODE;
        } else if (this.shootingMode instanceof DoubleShootingMode){
            this.shootingMode = SINGLE_SHOOTING_MODE;
        } else{
            ;
        }
    }

    public IShootingMode getShootingMode(){
        return shootingMode;
    }

    public abstract int getPower();

    public abstract double getAngle();

    public abstract void setPower(int p);

    public abstract void setAngle(double a);


    @Override
    public void acceptVisitor(IGameObjectsVisitor visitor) {
        visitor.visitCannon(this);
    }
}
