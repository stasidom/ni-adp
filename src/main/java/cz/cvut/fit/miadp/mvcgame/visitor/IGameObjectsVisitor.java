package cz.cvut.fit.miadp.mvcgame.visitor;

import cz.cvut.fit.miadp.mvcgame.model.gameObject.*;
import cz.cvut.fit.miadp.mvcgame.model.gameObject.familyB.Enemy_B;

public interface IGameObjectsVisitor {

    void visitCannon(AbsCannon cannon);
    void visitMissile(AbsMissile missile);
    void visitEnemy(AbsEnemy enemy);

    void visitGameInfo(AbsGameInfo absGameInfo);

    void visitCollision(AbsCollision absCollision);

    void visitGameOver(GameOver gameOver);

    void visitGameRunning(GameRunning gameRunning);

    void visitGameWin(GameWin gameWin);

    void visitEnemyB(Enemy_B enemy_b);
}
