package cz.cvut.fit.miadp.mvcgame.model.gameObject.familyA;

import cz.cvut.fit.miadp.mvcgame.abstractfactory.IGameObjectsFactory;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.Vector;
import cz.cvut.fit.miadp.mvcgame.model.gameObject.AbsCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObject.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.state.DoubleShootingMode;
import cz.cvut.fit.miadp.mvcgame.state.IShootingMode;
import cz.cvut.fit.miadp.mvcgame.state.SingleShootingMode;

import java.util.ArrayList;
import java.util.List;

public class Cannon_A extends AbsCannon {

    private IGameObjectsFactory goFact;
    private double initAngle;
    private int initVelocity;
    private List<AbsMissile> shootingBatch;



    public Cannon_A(Position initialPosition, IGameObjectsFactory goFact) {
        this.position = initialPosition;
        this.goFact = goFact;
        this.initAngle = MvcGameConfig.INIT_ANGLE;
        this.initVelocity = MvcGameConfig.INIT_POWER;
        this.shootingBatch = new ArrayList<AbsMissile>();
        this.shootingMode = new SingleShootingMode();
    }

    public int getPower() {
        return initVelocity;
    }

    public double getAngle() {
        return initAngle;
    }

    @Override
    public void setPower(int p) {
        this.initVelocity = p;
    }

    @Override
    public void setAngle(double a) {
        this.initAngle = a;
    }

    public void moveUp() {
        this.move(new Vector(0, -1 * MvcGameConfig.MOVE_STEP));
    }

    public void moveDown() {
        this.move(new Vector(0, MvcGameConfig.MOVE_STEP));
    }

    public void primitiveShoot() {
        this.shootingBatch.add( this.goFact.createMissile(this.initAngle, this.initVelocity));
    }

    @Override
    public List<AbsMissile> shoot() {

        this.shootingBatch.clear();

//      State to shoot
        this.shootingMode.shoot(this);

        return this.shootingBatch;
    }

    @Override
    public void aimDown() {
        this.initAngle -= MvcGameConfig.ANGLE_STEP;
    }

    @Override
    public void aimUp() {
        this.initAngle += MvcGameConfig.ANGLE_STEP;
    }

    @Override
    public void powerUp() {
        this.initVelocity += MvcGameConfig.POWER_STEP;
    }

    @Override
    public void powerDown() {
        this.initVelocity -= MvcGameConfig.POWER_STEP;
    }
}
