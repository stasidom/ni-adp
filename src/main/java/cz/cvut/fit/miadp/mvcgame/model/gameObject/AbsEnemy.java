package cz.cvut.fit.miadp.mvcgame.model.gameObject;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.Vector;
import cz.cvut.fit.miadp.mvcgame.visitor.IGameObjectsVisitor;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public abstract class AbsEnemy extends LifeTimeLimitedGameObject {


    @Override
    public void acceptVisitor(IGameObjectsVisitor visitor) {
        visitor.visitEnemy(this);
    }

    public AbsEnemy(Position p) {
        super(p);
    }

    public void move(){
        this.move(new Vector(-1, 0));

    }
}
