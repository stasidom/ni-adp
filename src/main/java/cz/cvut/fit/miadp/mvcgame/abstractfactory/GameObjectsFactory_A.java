package cz.cvut.fit.miadp.mvcgame.abstractfactory;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObject.*;
import cz.cvut.fit.miadp.mvcgame.model.gameObject.familyA.*;
import cz.cvut.fit.miadp.mvcgame.model.gameObject.familyB.Enemy_B;
import cz.cvut.fit.miadp.mvcgame.strategy.SimpleMoveStrategy;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameObjectsFactory_A implements IGameObjectsFactory {

    IGameModel model;

    public GameObjectsFactory_A(IGameModel model){
        this.model = model;
    }

    @Override
    public AbsGameInfo createGameInfo() {
        return new GameInfo_A(model, new Position(MvcGameConfig.GAME_INFO_X, MvcGameConfig.GAME_INFO_Y));
    }

    @Override
    public Cannon_A createCannon() {
        return new Cannon_A(new Position(MvcGameConfig.CANNON_POS_X, MvcGameConfig.CANNON_POS_Y), this);
    }

    @Override
    public Missile_A createMissile(double initAngle, int initVelocity) {
        return new Missile_A(new Position(model.getCannonPosition().getX(), model.getCannonPosition().getY()), this.model.getMoveStrategy(), initAngle, initVelocity);
    }

    @Override
    public List<AbsEnemy> createEnemies() {

        List<AbsEnemy> enemies = new ArrayList<>();
        for (int i = 0; i < MvcGameConfig.MAX_ENEMIES; i++){
            Random rand = new Random();

            int y = rand.nextInt( MvcGameConfig.MAX_Y - 2*MvcGameConfig.GAME_INFO_Y) + MvcGameConfig.GAME_INFO_Y;
            int x = MvcGameConfig.MAX_X/2 + rand.nextInt( MvcGameConfig.MAX_X/2 );

            if(i%2 == 0)
                enemies.add(new Enemy_A(new Position(x, y)));
            else
                enemies.add(new Enemy_B(new Position(x, y)));
        }


        return enemies;
    }

    @Override
    public AbsCollision createCollision(Position position) {
        return new Collision_A(position);
    }
}
