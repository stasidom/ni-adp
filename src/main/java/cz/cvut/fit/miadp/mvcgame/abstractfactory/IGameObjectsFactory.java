package cz.cvut.fit.miadp.mvcgame.abstractfactory;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObject.*;

import java.util.List;

public interface IGameObjectsFactory {

    AbsCannon createCannon();
    AbsMissile createMissile(double initAngle, int initVelocity);
    List<AbsEnemy> createEnemies();
    AbsGameInfo createGameInfo();

    AbsCollision createCollision(Position position);
}
