package cz.cvut.fit.miadp.mvcgame.model.gameObject.familyA;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObject.AbsGameInfo;
import cz.cvut.fit.miadp.mvcgame.proxy.GameModelProxy;

public class GameInfo_A extends AbsGameInfo {

    private IGameModel model;

    public GameInfo_A(IGameModel model, Position p) {
        super(p);
        this.model = model;
    }

    @Override
    public String getGameInfoText() {

        String gameInfo = "Lives: " + this.model.getLives()
                +"Score: " + this.model.getScore()
                +" Enemies: " + this.model.getEnemies().size()
                +" Cannon.Y: " + this.model.getCannonPosition().getY()
                +" Cannon Angle: " + this.model.getCannon().getAngle()
                +" Cannon Power: " + this.model.getCannon().getPower()
                +" Missiles: " + this.model.getMissiles().size()
                +" Move strategy: " + this.model.getMoveStrategy().getName()
                +" ShootMode: " + this.model.getCannon().getShootingMode().getName();

        return gameInfo;
    }
}
