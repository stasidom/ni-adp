package cz.cvut.fit.miadp.mvcgame.strategy;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.Vector;
import cz.cvut.fit.miadp.mvcgame.model.gameObject.AbsMissile;

public class RealisticMoveStrategy implements IMovingStrategy {

    @Override
    public String getName() {
        return "Realistic";
    }

    @Override
    public void updatePosition(AbsMissile missile) {

        float initVelocity = missile.getInitVelocity();
        double initAngle = missile.getInitAngle();
        float timeOfLive = missile.getAgeMillis()/(1000.0f); //secs

        int x = (int)(initVelocity*(Math.cos(initAngle)));
        int y = (int)((initVelocity*Math.sin(-initAngle) + 0.5*MvcGameConfig.GRAVITY*timeOfLive*timeOfLive));


        missile.move(new Vector(x, y));
    }
}
