package cz.cvut.fit.miadp.mvcgame.view;

import cz.cvut.fit.miadp.mvcgame.bridge.IGameGraphics;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.controller.GameController;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.model.gameObject.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.model.gameObject.GameObject;
import cz.cvut.fit.miadp.mvcgame.observer.IObserver;
import cz.cvut.fit.miadp.mvcgame.visitor.GameObjectsRender;


public class GameView implements IObserver {

    private GameController controller;
    private IGameModel model;
    private IGameGraphics gr;
    private GameObjectsRender render;
    private int updateCnt;


    public GameView(IGameModel model) {
        this.controller = new GameController(model);
        this.model = model;
        this.gr = null;
        updateCnt = 1;
        this.model.registerObserver(this);
        this.render = new GameObjectsRender();
    }


    public GameController getController() {
        return controller;
    }

    public void setGraphicContext(IGameGraphics gr){
        this.gr = gr;
        this.render.setGraphicsContext(gr);
    }

    public void render(){

        if(this.gr == null){return;}
        if(this.updateCnt > 0){
            gr.clear();

            for (GameObject go: this.model.getGameObjects()
                 ) {
                go.acceptVisitor(this.render);
            }

            this.updateCnt = 0;
        }
    }


    @Override
    public void update() {
        this.updateCnt++;
    }

}
