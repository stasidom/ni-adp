package cz.cvut.fit.miadp.mvcgame;

import java.util.List;
import java.util.Set;

import cz.cvut.fit.miadp.mvcgame.bridge.IGameGraphics;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.controller.GameController;
import cz.cvut.fit.miadp.mvcgame.memento.CareTaker;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.proxy.GameModelProxy;
import cz.cvut.fit.miadp.mvcgame.view.GameView;
import javafx.scene.input.KeyEvent;

public class MvcGame
{


    private IGameModel model;
    private GameView view;
    private GameController controller;

    public void init()
    {

        this.model = new GameModelProxy(new GameModel());
        this.view = new GameView(model);
        controller = view.getController();
        CareTaker.getInstance().setModel(model);
    }

    public void processPressedKeys(List<String> keyEvents)
    {
        //System.out.println("Size of pressed keys: " + keyEvents.size());
        this.controller.processPressedKeys(keyEvents);
    }

    public void update()
    {
        // nothing yet
//        this.model.update();
        this.model.timeTick();
    }

    public void render(IGameGraphics gr)
    {
        this.view.setGraphicContext(gr);
        this.view.render();

    }

    public String getWindowTitle()
    {
        return "ANGRY BIRDS FOR NI-ADP";
    }

    public int getWindowWidth()
    {
        return MvcGameConfig.MAX_X;
    }

    public int getWindowHeight()
    {
        return  MvcGameConfig.MAX_Y;
    }
}