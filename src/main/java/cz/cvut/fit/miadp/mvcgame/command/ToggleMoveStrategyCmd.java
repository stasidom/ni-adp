package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.model.IGameModel;

public class ToggleMoveStrategyCmd extends AbstractGameCommand {

    public ToggleMoveStrategyCmd(IGameModel subject) {
        this.subject = subject;
    }

    @Override
    protected void execute() {
        this.subject.toggleMoveStrategy();
    }
}
