package cz.cvut.fit.miadp.mvcgame.model.gameObject.familyA;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObject.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.strategy.IMovingStrategy;
import cz.cvut.fit.miadp.mvcgame.strategy.SimpleMoveStrategy;

public class Missile_A extends AbsMissile {

    private IMovingStrategy moveStrategy;

    public Missile_A(Position initialPosition, IMovingStrategy moveStrategy, double initAngle, int initVelocity) {
        super(initialPosition, initVelocity, initAngle);
        this.moveStrategy = moveStrategy;
    }

    @Override
    public void move() {
        this.moveStrategy.updatePosition(this);
    }
}
