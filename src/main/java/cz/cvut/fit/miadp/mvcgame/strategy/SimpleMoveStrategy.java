package cz.cvut.fit.miadp.mvcgame.strategy;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObject.AbsMissile;

public class SimpleMoveStrategy implements IMovingStrategy {

    @Override
    public String getName() {
        return "Simple";
    }

    @Override
    public void updatePosition(AbsMissile missile) {


        double angle =  missile.getInitAngle();
        float power = missile.getInitVelocity();
        long timeOfLive = missile.getAgeSeconds();



        int x = (int)(missile.getPosition().getX() + power*(Math.cos(angle)));
        int y = (int)(missile.getPosition().getY() + power*(Math.sin(-angle)));

        //System.out.println("Angle: " + angle + "Power: " + power);
       // System.out.println("Move x: " + x + "Move y: " + y);

        missile.move(new Position(x, y));
    }
}
