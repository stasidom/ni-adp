package cz.cvut.fit.miadp.mvcgame.model.gameObject;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.Vector;
import cz.cvut.fit.miadp.mvcgame.visitor.GameObjectsRender;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitable;

public abstract class GameObject implements IVisitable {

    protected Position position;

    public void move(Vector v){
        this.position.add(v);
    }

    public void move(Position p){
        this.position = p;
    }

    public Position getPosition(){
        return this.position;
    }

    public boolean isCollide(GameObject m){
        boolean collision = false;

        int aX = this.position.getX();
        int aY = this.position.getY();

        int bX = m.position.getX();
        int bY = m.position.getY();

        collision = (Math.abs(aX-bX) < MvcGameConfig.COLLIDE_FACTOR)
                && (Math.abs(aY-bY) < MvcGameConfig.COLLIDE_FACTOR);

        return collision;


    }
}
