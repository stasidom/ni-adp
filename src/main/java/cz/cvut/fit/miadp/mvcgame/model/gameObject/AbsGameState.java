package cz.cvut.fit.miadp.mvcgame.model.gameObject;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.visitor.IGameObjectsVisitor;

public abstract class AbsGameState extends GameObject {


    public AbsGameState(Position initialPosition) {
        this.position = initialPosition;
    }

}
