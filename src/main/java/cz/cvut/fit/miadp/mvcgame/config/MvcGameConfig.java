package cz.cvut.fit.miadp.mvcgame.config;

public class MvcGameConfig 
{
    public static final int MAX_X = 1280;
    public static final int MAX_Y = 720;
    public static final int MOVE_STEP = 10;
    public static final int CANNON_POS_X = 50;
    public static final int CANNON_POS_Y = MAX_Y/2;
    public static final long TIME_TICK_PERIOD = 1000;
    public static final float GRAVITY = 15.0f;
    public static final int INIT_POWER = 10;
    public static final double INIT_ANGLE = 0;
    public static final double ANGLE_STEP = Math.PI/18;
    public static final int POWER_STEP = 2;
    public static final int COLLIDE_FACTOR = 20;

    public static final float POWER_DIVISOR = 20.0f;
    public static final int MAX_ENEMIES = 20;
    public static final int GAME_INFO_X = 20;
    public static final int GAME_INFO_Y = 20;

    public static final long COLLISION_LIFETIME = 1;
    public static final int SAFE_AREA = 50;
}