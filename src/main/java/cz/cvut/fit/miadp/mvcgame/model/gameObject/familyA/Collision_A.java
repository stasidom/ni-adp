package cz.cvut.fit.miadp.mvcgame.model.gameObject.familyA;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObject.AbsCollision;

public class Collision_A extends AbsCollision {

    public Collision_A(Position p) {
        super(p);
    }
}
