package cz.cvut.fit.miadp.mvcgame.controller;

import com.sun.tools.javac.Main;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;

import java.io.*;
import javax.sound.sampled.*;

public class SoundController {

    final String collision = "/sounds/coin.wav";
    final String shot = "/sounds/pop.wav";


    public static void playSound(String path) {
        Clip clip;
        try {
            URL url = SoundController.class.getResource(path);
            AudioInputStream input = AudioSystem.getAudioInputStream(url);
            clip = AudioSystem.getClip();
            clip.open(input);
            clip.start();
        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
            e.printStackTrace();
        }
    }

}

