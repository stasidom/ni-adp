package cz.cvut.fit.miadp.mvcgame.model.gameObject.familyB;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.Vector;
import cz.cvut.fit.miadp.mvcgame.model.gameObject.AbsEnemy;
import cz.cvut.fit.miadp.mvcgame.visitor.IGameObjectsVisitor;

public class Enemy_B extends AbsEnemy {

    public Enemy_B(Position p) {
        super(p);
    }

    @Override
    public void acceptVisitor(IGameObjectsVisitor visitor) {
        visitor.visitEnemyB(this);
    }

    @Override
    public void move() {
        this.move(new Vector(-2, 0));
    }
}
