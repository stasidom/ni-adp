package cz.cvut.fit.miadp.mvcgame.model.gameObject;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.visitor.IGameObjectsVisitor;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public abstract class LifeTimeLimitedGameObject extends GameObject {

    private LocalDateTime bornAt;

    public LifeTimeLimitedGameObject(Position p) {
        this.position = p;
        this.bornAt = LocalDateTime.now();
    }

    public long getAgeSeconds() {
        LocalDateTime now = LocalDateTime.now();

        return ChronoUnit.SECONDS.between(this.bornAt, now);
    }

    public float getAgeMillis() {
        LocalDateTime now = LocalDateTime.now();

        return ChronoUnit.MILLIS.between(this.bornAt, now);
    }


}
