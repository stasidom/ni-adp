package cz.cvut.fit.miadp.mvcgame.strategy;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObject.AbsMissile;

public interface IMovingStrategy {

    public void updatePosition(AbsMissile missile);
    public String getName();

}
