package cz.cvut.fit.miadp.mvcgame.proxy;

import cz.cvut.fit.miadp.mvcgame.command.AbstractGameCommand;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObject.AbsCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObject.AbsEnemy;
import cz.cvut.fit.miadp.mvcgame.model.gameObject.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.model.gameObject.GameObject;
import cz.cvut.fit.miadp.mvcgame.observer.IObserver;
import cz.cvut.fit.miadp.mvcgame.strategy.IMovingStrategy;

import java.util.List;

public class GameModelProxy implements IGameModel {

    private IGameModel subject;

    public GameModelProxy(IGameModel model) {
        this.subject = model;
    }

    @Override
    public List<AbsEnemy> getEnemies() {
        return this.subject.getEnemies();
    }

    @Override
    public int getLives() {
        return this.subject.getLives();
    }

    @Override
    public void restart() {
        this.subject.restart();
    }

    @Override
    public AbsCannon getCannon() {
        return this.subject.getCannon();
    }

    @Override
    public List<AbsMissile> getMissiles() {
        return this.subject.getMissiles();
    }

    @Override
    public int getScore() {
        return this.subject.getScore();
    }

    @Override
    public void moveCannonDown() {
        this.subject.moveCannonDown();
    }

    @Override
    public void moveCannonUp() {
        this.subject.moveCannonUp();
    }

    @Override
    public void cannonShoot() {
        this.subject.cannonShoot();
    }

    @Override
    public void cannonAimUp() {
        this.subject.cannonAimUp();
    }

    @Override
    public void cannonAimDown() {
        this.subject.cannonAimDown();
    }

    @Override
    public void cannonPowerUp() {
        this.subject.cannonPowerUp();
    }

    @Override
    public void cannonPowerDown() {
        this.subject.cannonPowerDown();
    }

    @Override
    public void toggleMoveStrategy() {
        this.subject.toggleMoveStrategy();
    }

    @Override
    public void update() {
        this.subject.update();
    }

    @Override
    public void timeTick() {
        this.subject.timeTick();
    }

    @Override
    public List<GameObject> getGameObjects() {
        return this.subject.getGameObjects();
    }

    @Override
    public IMovingStrategy getMoveStrategy() {
        return this.subject.getMoveStrategy();
    }

    @Override
    public Object createMemento() {
        return this.subject.createMemento();
    }

    @Override
    public void setMemento(Object memento) {
        this.subject.setMemento(memento);
    }

    @Override
    public Position getCannonPosition() {
        return this.subject.getCannonPosition();
    }

    @Override
    public void registerObserver(IObserver o) {
        this.subject.registerObserver(o);
    }

    @Override
    public void unregisterObserver(IObserver o) {
        this.subject.unregisterObserver(o);
    }

    @Override
    public void notifyObservers() {
        this.subject.notifyObservers();
    }

    @Override
    public void toggleShootingMode() {
        this.subject.toggleShootingMode();
    }

    @Override
    public void registerCommand(AbstractGameCommand cmd) {
        this.subject.registerCommand(cmd);
    }

    @Override
    public void undoLastCommand() {
        this.subject.undoLastCommand();
    }
}
