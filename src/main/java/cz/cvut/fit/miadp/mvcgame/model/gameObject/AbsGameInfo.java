package cz.cvut.fit.miadp.mvcgame.model.gameObject;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.visitor.IGameObjectsVisitor;

public abstract class AbsGameInfo extends GameObject {

    public AbsGameInfo(Position p) {
        this.position = p;
    }

    @Override
    public void acceptVisitor(IGameObjectsVisitor visitor) {
        visitor.visitGameInfo(this);
    }

    public abstract String getGameInfoText();
}
