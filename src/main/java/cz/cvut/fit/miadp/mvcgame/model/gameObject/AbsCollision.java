package cz.cvut.fit.miadp.mvcgame.model.gameObject;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.visitor.IGameObjectsVisitor;

public abstract class AbsCollision extends LifeTimeLimitedGameObject {

    public AbsCollision(Position p) {
        super(p);
    }

    @Override
    public void acceptVisitor(IGameObjectsVisitor visitor) {
        visitor.visitCollision(this);
    }
}
