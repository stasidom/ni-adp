package cz.cvut.fit.miadp.mvcgame.model.gameObject.familyA;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObject.AbsEnemy;

public class Enemy_A extends AbsEnemy {

    public Enemy_A(Position initialPosition) {
        super(initialPosition);
    }

}
