package cz.cvut.fit.miadp.mvcgame.model;

import cz.cvut.fit.miadp.mvcgame.command.AbstractGameCommand;
import cz.cvut.fit.miadp.mvcgame.model.gameObject.AbsCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObject.AbsEnemy;
import cz.cvut.fit.miadp.mvcgame.model.gameObject.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.model.gameObject.GameObject;
import cz.cvut.fit.miadp.mvcgame.observer.IObservable;
import cz.cvut.fit.miadp.mvcgame.state.IShootingMode;
import cz.cvut.fit.miadp.mvcgame.strategy.IMovingStrategy;

import java.util.Collection;
import java.util.List;

public interface IGameModel extends IObservable {


    public AbsCannon getCannon();
    public List<AbsMissile> getMissiles();
    public int getScore();

    public void moveCannonDown();
    public void moveCannonUp();
    public void cannonShoot();

    public void cannonAimUp();
    public void cannonAimDown();
    public void cannonPowerUp();
    public void cannonPowerDown();
    public void toggleMoveStrategy();

    public void update();
    public void timeTick();
    public List<GameObject> getGameObjects();
    public IMovingStrategy getMoveStrategy();
    public Object createMemento();
    public void setMemento(Object memento);
    public Position getCannonPosition();
    public void toggleShootingMode();

    public void registerCommand(AbstractGameCommand cmd);
    public void undoLastCommand();

    public List<AbsEnemy> getEnemies();

    public  int getLives();

    public void restart();


//TODO finish this
}
