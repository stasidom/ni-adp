package cz.cvut.fit.miadp.mvcgame.model;

import cz.cvut.fit.miadp.mvcgame.abstractfactory.GameObjectsFactory_A;
import cz.cvut.fit.miadp.mvcgame.abstractfactory.IGameObjectsFactory;
import cz.cvut.fit.miadp.mvcgame.command.AbstractGameCommand;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.controller.SoundController;
import cz.cvut.fit.miadp.mvcgame.model.gameObject.*;
import cz.cvut.fit.miadp.mvcgame.model.gameObject.familyA.Enemy_A;
import cz.cvut.fit.miadp.mvcgame.observer.IObservable;
import cz.cvut.fit.miadp.mvcgame.observer.IObserver;
import cz.cvut.fit.miadp.mvcgame.state.IShootingMode;
import cz.cvut.fit.miadp.mvcgame.strategy.IMovingStrategy;
import cz.cvut.fit.miadp.mvcgame.strategy.RealisticMoveStrategy;
import cz.cvut.fit.miadp.mvcgame.strategy.SimpleMoveStrategy;

import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;

public class GameModel implements IGameModel, IObservable {

    private AbsCannon cannon;
    private List<AbsMissile> missiles;
    private ArrayList<IObserver> observers;
    private IGameObjectsFactory goFact;
    private IMovingStrategy moveStrategy;
    private AbsGameInfo gameInfo;
    private List<AbsCollision> collisions;
    private List<AbsEnemy> enemies;
    private int score = 0;
    private int lives = 3;
    //SoundController soundController;

    private Queue<AbstractGameCommand> unexecutedCmds = new LinkedBlockingQueue<>();
    private Stack<AbstractGameCommand> executedCmds = new Stack<AbstractGameCommand>();



    public GameModel() {
        observers = new ArrayList<>();
        this.missiles = new ArrayList<AbsMissile>();
        this.goFact = new GameObjectsFactory_A(this);
        this.cannon = this.goFact.createCannon();
        this.moveStrategy = new SimpleMoveStrategy();
        this.gameInfo = this.goFact.createGameInfo();

        this.enemies = new ArrayList<>();
        this.enemies.addAll(goFact.createEnemies());

        this.collisions = new ArrayList<>();

    }

    @Override
    public int getLives() {
        return lives;
    }

    @Override
    public List<AbsEnemy> getEnemies() {
        return enemies;
    }

    public AbsCannon getCannon(){
        return cannon;
    }

    @Override
    public int getScore() {
        return this.score;
    }

    public List<AbsMissile> getMissiles() {
        return this.missiles;
    }

    public Position getCannonPosition() {
        return this.cannon.getPosition();
    }

    public void moveCannonDown() {

        cannon.moveDown();

        this.notifyObservers();
    }

    public void moveCannonUp() {
        cannon.moveUp();

        this.notifyObservers();
    }

    public void cannonShoot() {
         this.missiles.addAll(this.cannon.shoot());
         SoundController.playSound("/sounds/pop.wav");
        this.notifyObservers();
    }

    public void cannonAimUp(){
        this.cannon.aimUp();

        this.notifyObservers();
    }

    public void cannonAimDown(){
        this.cannon.aimDown();

        this.notifyObservers();
    }

    public void cannonPowerUp(){
        this.cannon.powerUp();

        this.notifyObservers();
    }

    public void cannonPowerDown(){
        this.cannon.powerDown();

        this.notifyObservers();
    }

    public void toggleShootingMode(){
        this.cannon.toggleShootingMode();
    }


    public void timeTick() {
        this.update();
    }

    public void update() {

            this.executedCmds();
            this.moveMissiles();
            this.moveEnemies();
            this.handleCollisions();

    }


    private void moveEnemies() {

        for (AbsEnemy e: enemies
             ) {
            e.move();
        }
        this.destroyEnemies();

        this.notifyObservers();

    }

    public void handleCollisions(){

        List<AbsEnemy> enemiesToDelete = new ArrayList<>();
        List<AbsMissile> missilesToDelete = new ArrayList<>();
        List<AbsCollision> collsToDelete= new ArrayList<AbsCollision>();

        for (AbsEnemy e: enemies
             ) {
            for (AbsMissile m: missiles
            ) {
               if(e.isCollide(m)){
                    collisions.add(this.goFact.createCollision(e.getPosition()));
                    enemiesToDelete.add(e);
                    missilesToDelete.add(m);
                    this.score++;
                   SoundController.playSound("/sounds/coin.wav");
                   notifyObservers();
               }
            }
        }

        for(AbsCollision c : collisions)
        {
            if(c.getAgeSeconds() > MvcGameConfig.COLLISION_LIFETIME)
            {
                collsToDelete.add(c);
            }
        }

        for (AbsEnemy e: enemiesToDelete
             ) {
            enemies.remove(e);
        }
        for (AbsMissile m: missilesToDelete
        ) {
            missiles.remove(m);
        }
        for (AbsCollision c: collsToDelete
        ) {
            collisions.remove(c);
        }

    }

    public void destroyEnemies(){
        List<AbsEnemy> toRemove = new ArrayList<AbsEnemy>();
        for (AbsEnemy e : this.enemies
        ) {
            if (e.getPosition().getX() < MvcGameConfig.SAFE_AREA) {
                toRemove.add(e);
                this.lives--;
            }
        }

        this.enemies.removeAll(toRemove);
    }

    public void destroyMissiles() {
        List<AbsMissile> toRemove = new ArrayList<AbsMissile>();
        for (AbsMissile m : this.missiles
        ) {
            if (m.getPosition().getX() > MvcGameConfig.MAX_X) {
                toRemove.add(m);
            }
        }

        this.missiles.removeAll(toRemove);
    }

    public void moveMissiles() {
        for (AbsMissile m : this.missiles
        ) {
             m.move();
        }
        this.destroyMissiles();

        this.notifyObservers();
    }


    @Override
    public void registerObserver(IObserver obs) {
        if (!observers.contains(obs))
            observers.add(obs);
    }

    @Override
    public void unregisterObserver(IObserver obs) {
        if (observers.contains(obs))
            observers.remove(obs);
    }

    @Override
    public void notifyObservers() {
        for (IObserver o : this.observers
        ) {
            o.update();
        }
    }

    @Override
    public void restart() {
        this.missiles = new ArrayList<AbsMissile>();
        this.cannon = this.goFact.createCannon();
        this.moveStrategy = new SimpleMoveStrategy();
        this.gameInfo = this.goFact.createGameInfo();

        this.score = 0;
        this.lives = 3;

        this.enemies = new ArrayList<>();
        this.enemies.addAll(goFact.createEnemies());


        this.collisions = new ArrayList<>();
    }

    public List<GameObject> getGameObjects() {
        List<GameObject> gameObjects = new ArrayList<GameObject>();

        if(lives <= 0){
            gameObjects.add(new GameOver(new Position(350,200 )));
        } else if ((lives>0) && (enemies.size()==0)){
            gameObjects.add(new GameWin(new Position(400,200 )));
        } else {
            gameObjects.addAll(missiles);
            gameObjects.add(cannon);
            gameObjects.addAll(enemies);
            gameObjects.add(gameInfo);
            gameObjects.addAll(collisions);
        }
        return gameObjects;
    }


    public void toggleMoveStrategy() {

        if (this.moveStrategy instanceof SimpleMoveStrategy) {
            this.moveStrategy = new RealisticMoveStrategy();
        } else if (this.moveStrategy instanceof RealisticMoveStrategy){
            this.moveStrategy = new SimpleMoveStrategy();
        } else{
           ;
        }

        System.out.println(moveStrategy.toString());
    }

    public IMovingStrategy getMoveStrategy() {
        return this.moveStrategy;
    }


    private class Memento{
        private int score;
        private int cannonX;
        private int cannonY;
        private double cannonAngle;
        private int cannonPower;
        private List<AbsEnemy> enemies = new ArrayList<>();
        private IMovingStrategy moveStrategy;
        private IShootingMode shootingMode;
        //TODO GameMoodel state snapshot


    }

    public Object createMemento(){
        Memento m = new Memento();
        m.score = this.score;
        m.cannonX = this.getCannonPosition().getX();
        m.cannonY = this.getCannonPosition().getY();
        m.cannonAngle = this.cannon.getAngle();
        m.cannonPower = this.cannon.getPower();
        m.moveStrategy = this.moveStrategy;
        m.enemies.addAll(this.enemies);
        m.shootingMode = this.cannon.getShootingMode();

        return m;
    }

    public void setMemento (Object memento){
        Memento m = (Memento)memento;
        this.score = m.score;
        this.cannon.getPosition().setX(m.cannonX);
        this.cannon.getPosition().setY(m.cannonY);
        this.moveStrategy = m.moveStrategy;
        this.enemies = m.enemies;
        this.cannon.setAngle(m.cannonAngle);
        this.cannon.setPower(m.cannonPower);

        if(!cannon.getShootingMode().getName().equals(m.shootingMode.getName()))
            this.cannon.toggleShootingMode();
    }

    public void registerCommand(AbstractGameCommand cmd) {
        this.unexecutedCmds.add(cmd);
    }

    @Override
    public void undoLastCommand() {
        if(executedCmds.isEmpty())
            return;
        AbstractGameCommand cmd = this.executedCmds.pop();
        cmd.uniExecute();

        this.notifyObservers();

    }

    private void executedCmds() {
        while(!this.unexecutedCmds.isEmpty()){
            AbstractGameCommand cmd = this.unexecutedCmds.poll();
            cmd.doExecute();

            this.executedCmds.push(cmd);
        }
    }
}
