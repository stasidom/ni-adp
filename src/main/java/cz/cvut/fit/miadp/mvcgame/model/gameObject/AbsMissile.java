package cz.cvut.fit.miadp.mvcgame.model.gameObject;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.Vector;
import cz.cvut.fit.miadp.mvcgame.strategy.IMovingStrategy;
import cz.cvut.fit.miadp.mvcgame.visitor.IGameObjectsVisitor;

public abstract class AbsMissile extends LifeTimeLimitedGameObject {

    private float initVelocity;
    private double initAngle;

    public AbsMissile(Position p, float initVelocity, double initAngle) {
        super(p);
        this.initVelocity = initVelocity;
        this.initAngle = initAngle;
    }

    @Override
    public void acceptVisitor(IGameObjectsVisitor visitor) {
        visitor.visitMissile(this);
    }

    public abstract void move();

    public float getInitVelocity() {
        return this.initVelocity;
    }

    public double getInitAngle() {
        return this.initAngle;
    }
}
