package cz.cvut.fit.miadp.mvcgame.visitor;

import cz.cvut.fit.miadp.mvcgame.bridge.IGameGraphics;
import cz.cvut.fit.miadp.mvcgame.model.gameObject.*;
import cz.cvut.fit.miadp.mvcgame.model.gameObject.familyA.Collision_A;
import cz.cvut.fit.miadp.mvcgame.model.gameObject.familyB.Enemy_B;

public class GameObjectsRender implements IGameObjectsVisitor {
    private IGameGraphics gr;

    public void setGraphicsContext(IGameGraphics gr) {
        this.gr = gr;
    }

    @Override
    public void visitCannon(AbsCannon cannon) {
        this.gr.drawImage("images/cannon.png", cannon.getPosition());
    }

    @Override
    public void visitMissile(AbsMissile missile) {
        this.gr.drawImage("images/missile.png", missile.getPosition());
    }

    @Override
    public void visitEnemy(AbsEnemy enemy) {
        this.gr.drawImage("images/enemy1.png", enemy.getPosition());
    }

    @Override
    public void visitEnemyB(Enemy_B enemy_b) {
        this.gr.drawImage("images/enemy2.png", enemy_b.getPosition());
    }

    @Override
    public void visitGameInfo(AbsGameInfo absGameInfo) {
        this.gr.drawText(absGameInfo.getGameInfoText(), absGameInfo.getPosition());
    }

    @Override
    public void visitCollision(AbsCollision absCollision) {
        this.gr.drawImage("images/collision.png", absCollision.getPosition());
    }

    @Override
    public void visitGameOver(GameOver gameOver) {
        this.gr.drawImage("images/youlose.png", gameOver.getPosition());
    }

    @Override
    public void visitGameRunning(GameRunning gameRunning) {
        //do nothing, because game is running
    }

    @Override
    public void visitGameWin(GameWin gameWin) {
        this.gr.drawImage("images/youwin.png", gameWin.getPosition());
    }
}
