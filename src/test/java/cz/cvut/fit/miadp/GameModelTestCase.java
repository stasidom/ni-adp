package cz.cvut.fit.miadp;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.strategy.IMovingStrategy;
import cz.cvut.fit.miadp.mvcgame.strategy.RealisticMoveStrategy;
import cz.cvut.fit.miadp.mvcgame.strategy.SimpleMoveStrategy;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class GameModelTestCase {
    IGameModel model;

    @Before
    public void initial() {
        model = new GameModel();
    }

    @Test
    public void testStrategy() {
        IMovingStrategy strategy = model.getMoveStrategy();
        IMovingStrategy simple = new SimpleMoveStrategy();
        Assert.assertTrue(strategy.getName().equals(simple.getName()));

        IMovingStrategy real = new RealisticMoveStrategy();
        model.toggleMoveStrategy();
        strategy = model.getMoveStrategy();
        Assert.assertTrue(strategy.getName().equals(real.getName()));

        model.toggleMoveStrategy();
        strategy = model.getMoveStrategy();
        Assert.assertTrue(strategy.getName().equals(simple.getName()));
    }

    @Test
    public void testScore() {
        Assert.assertEquals(model.getScore(), 0);
    }

    @Test
    public void testMoveCannonUp() {
        Assert.assertEquals(model.getCannonPosition().getY(), MvcGameConfig.CANNON_POS_Y);

        model.moveCannonUp();

        Assert.assertEquals(model.getCannonPosition().getY(), MvcGameConfig.CANNON_POS_Y-10);

        model.moveCannonUp();
        model.moveCannonUp();

        Assert.assertEquals(model.getCannonPosition().getY(), MvcGameConfig.CANNON_POS_Y-30);
    }

    @Test
    public void testMoveCannonDown() {
        Assert.assertEquals(model.getCannonPosition().getY(), MvcGameConfig.CANNON_POS_Y);

        model.moveCannonDown();
        model.moveCannonDown();
        model.moveCannonDown();
        model.moveCannonDown();
        model.moveCannonDown();

        Assert.assertEquals(model.getCannonPosition().getY(), MvcGameConfig.CANNON_POS_Y+50);
    }
}


