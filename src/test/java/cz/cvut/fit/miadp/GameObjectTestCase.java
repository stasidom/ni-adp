package cz.cvut.fit.miadp;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.model.gameObject.AbsCannon;
import cz.cvut.fit.miadp.mvcgame.proxy.GameModelProxy;
import cz.cvut.fit.miadp.mvcgame.state.DoubleShootingMode;
import cz.cvut.fit.miadp.mvcgame.state.SingleShootingMode;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class GameObjectTestCase {
    IGameModel model;
    IGameModel modelProxy;
    AbsCannon cannon;

    @Before
    public void initial(){
        model = new GameModel();
        modelProxy = new GameModelProxy( model );
        cannon = model.getCannon();
    }

    @Test
    public void testCannonPosition(){
        Assert.assertEquals(cannon.getPosition().getX(), MvcGameConfig.CANNON_POS_X);
        Assert.assertEquals(cannon.getPosition().getY(), MvcGameConfig.CANNON_POS_Y);

        cannon.getPosition().setX(cannon.getPosition().getX() + 50);
        Assert.assertEquals(cannon.getPosition().getX(), MvcGameConfig.CANNON_POS_X+50);

        cannon.getPosition().setY(cannon.getPosition().getY() + MvcGameConfig.CANNON_POS_Y);
        Assert.assertEquals(cannon.getPosition().getY(), 2*MvcGameConfig.CANNON_POS_Y);
    }

    @Test
    public void testCannonAngle(){
        Assert.assertTrue(cannon.getAngle() == MvcGameConfig.INIT_ANGLE);

        cannon.aimDown();
        Assert.assertTrue(cannon.getAngle() == (MvcGameConfig.INIT_ANGLE-MvcGameConfig.ANGLE_STEP));

        cannon.aimUp();
        cannon.aimUp();
        Assert.assertTrue(cannon.getAngle() == (MvcGameConfig.INIT_ANGLE+MvcGameConfig.ANGLE_STEP));
    }

    @Test
    public void testCannonPower(){
        Assert.assertTrue(cannon.getPower() == MvcGameConfig.INIT_POWER);

        cannon.powerUp();
        cannon.powerUp();
        Assert.assertTrue(cannon.getPower() == (MvcGameConfig.INIT_POWER + 2*MvcGameConfig.POWER_STEP));

        cannon.powerDown();
        Assert.assertTrue(cannon.getPower() == (MvcGameConfig.INIT_POWER + MvcGameConfig.POWER_STEP));
    }

    @Test
    public void testCannonShootingMode(){
        Assert.assertTrue(cannon.getShootingMode().getName().equals(new SingleShootingMode().getName()));

        cannon.toggleShootingMode();
        Assert.assertTrue(cannon.getShootingMode().getName().equals(new DoubleShootingMode().getName()));

        cannon.toggleShootingMode();
        Assert.assertTrue(cannon.getShootingMode().getName().equals(new SingleShootingMode().getName()));
    }
}



